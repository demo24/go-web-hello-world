FROM golang

RUN mkdir /app
COPY hello_world.go /app/
WORKDIR /app
CMD ["go", "run", "hello_world.go"]
EXPOSE 8082
