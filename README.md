# go-web-hello-world

simple hello world demo with golang

Procedure for step 1-7:

1.
ssh sre@10.210.149.84
passwd input: ericsson

2.
Install gitlab on Ubuntu is exactly copy paste the command from the given link
Install gitlab on Mac is slightly different, command as follow

sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
gitlab-runner install
gitlab-runner start
brew install gitlab-runner
brew services start gitlab-runner

3.
Create a gitlab account
Create demo group and go-web-hello-world in the gitlab website after login
Setup ssh key for clone and commit from cmd line:
First, generate a ssh key by:
ssh-keygen -t ed25519 -C "<youremail@company.com>@gmail.com"
cat ~/.ssh/id_ed25519.pub, copy it and paste it to the ssh key in gitlab website
Now you could clone and commit with the following command:

git clone git@yoururl.git
git add "your files" -m "your comments"
git commit


4.
See hello_world.go file
To run the go file by:
go run hello_world.go
Open a browser and input http://<ipaddress>:8081
Expected results:
Go Web Hello World!
5.
I installed it in Mac, brew install docker
In Ubuntu I assume it's sudo apt-get install docker

6.
Edit the Dockerfile, check the Dockerfile in the same directory.
Build:
docker build -t go-demo .
running go-demo docker image by:

docker run -d -p 8082:8081 go-demo
Open a browser and input http://<youripaddress>:8082
Expected results:
Go Web Hello World!

7.
To create a docker hub account.
Do the following command in your teminal:

docker login --username=yourhubusername --email=youremail@company.com
docker tag go-demo <your-account-name>/go-web-hello-world:v0.1
docker push <your-account-name>/go-web-hello-world

Now you should find your image in https://hub.docker.com/repository/docker/<your-docker-hub-account>/go-web-hello-world

8.
9.
I do not have access to any Ubuntu machine, I only have Mac, I did not find any doc on how to install kubeadm on MacOs. I had kubemini and kubectl and etc. installed before.

10.
11.
Install kubernetes dashboard by running the following command:
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta6/aio/deploy/recommended.yaml

Then run  kubectl proxy
The dashboard could be seen at 

http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

12.
To get the token and login to the dashboard could be done with the following command:

kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep <your-user-name> | awk '{print $1}')

after running the above command, a token like the following will show up:

token:      eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJkZWZhdWx0LXRva2VuLXM1ZzZjIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImRlZmF1bHQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJjYzMzZTQwNS0xZGY4LTExZWEtOTRiOS0wMjUwMDAwMDAwMDEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZXJuZXRlcy1kYXNoYm9hcmQ6ZGVmYXVsdCJ9.qsRGp3BXXRKlV4GblH6z-tIZI2ndyOUrPZ14yjagUogaWpIXNCsm6Se-Pbk6kARBn43XOAkfdrWGCdZic_fPSzqVmAop-xw7bKnliTYQ_wUvmBgcnBOgNe3_szCBZzWEf-kbYgB3zGJcL3NLD-495l6LqQ0qxBHIeht0-sUUoNpdqA_6cgaIsqzARf-X50xteOzSgFn4jfr9TIhLtN_bELKKjE_5ojsM2GSS8la3MAtnrYYjiLqTl7u7dzjDhDSrq4J2azsf53Al1clQV2yB4R3jIsNmkujWzL40dg_NpD8TP7ulJAVIoCb34qxZGeHu72_rUavfWCtqgbBjJo52uA

Copy the token and open a browser type the url: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/login

paste the token in the second button, Token, will login to the dashboard, all the service and etc. could be seen now.
